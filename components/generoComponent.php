<?
namespace app\components;

use yii\base\Component;

class generoComponent extends Component{


    public static function genSelect($field,$default = false){

        $list = array(
            "M" => "Masculino",
            "F" => "Feminino",
            "O" => "Outros"
        );
        $estrutura = "<label for=\"{$field}\">Gênero</label>";
        
        // if($default != 'M' && $default != 'F'){
        //     $defaultx = 'O';
        //     $estrutura = "<select name=\"\" id=\"{$field}\" class=\"custom-select actionGenero\">
        //     <option value=\"\">Selecione seu gênero</option>";
        //     foreach($list as $ch=>$g){
        //         $estrutura .= "<option value=\"{$ch}\"".($ch == $defaultx ? ' selected' : '').">{$g}</option>";
        //     }
        
        //     $estrutura .= "</select>";
        //     $estrutura .= "<input class=\"form-control outroGenero\" type=\"text\" name=\"{$field}\" value=\"{$default}\">";
        // } else{            
        //     $estrutura .= "<select name=\"{$field}\" id=\"{$field}\" class=\"custom-select actionGenero\">
        //         <option value=\"\">Selecione seu gênero</option>";
        //         foreach($list as $ch=>$g){
        //             $estrutura .= "<option value=\"{$ch}\"".($ch == $default ? ' selected' : '').">{$g}</option>";
        //         }
            
        //     $estrutura .= "</select>";
        // }

        $defaultx = '';
        
        if($default != 'M' && $default != 'F'){
            $defaultx = 'O';
            $estrutura .= "<select name=\"\" id=\"{$field}\" class=\"custom-select actionGenero\">
            <option value=\"\">Selecione seu gênero</option>";
        } else{
            $estrutura .= "<select name=\"{$field}\" id=\"{$field}\" class=\"custom-select actionGenero\">
            <option value=\"\">Selecione seu gênero</option>";
        }

        foreach($list as $ch=>$g){
            $estrutura .= "<option value=\"{$ch}\"".($ch == $default || $ch == $defaultx ? ' selected' : '').">{$g}</option>";
        }
        

        if($default != 'M' && $default != 'F' && $default != ''){
            $estrutura .= "<input class=\"form-control outroGenero\" type=\"text\" name=\"{$field}\" value=\"{$default}\">";
        }
        $estrutura .= "</select>";

        return $estrutura;
    }
}
?>
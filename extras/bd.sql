-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para jp_php
CREATE DATABASE IF NOT EXISTS `jp_php` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `jp_php`;

-- Copiando estrutura para tabela jp_php.jp_administradora
CREATE TABLE IF NOT EXISTS `jp_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(255) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_administradora: ~9 rows (aproximadamente)
DELETE FROM `jp_administradora`;
/*!40000 ALTER TABLE `jp_administradora` DISABLE KEYS */;
INSERT INTO `jp_administradora` (`id`, `nomeAdm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'AdmBolada', '18039971000100', '2022-03-30 13:07:55'),
	(2, 'GridAdministra', '18039971000100', '2022-03-30 13:08:18'),
	(3, 'FlucaAdm', '92920057000111', '2022-03-30 13:08:58'),
	(6, 'TesteAdm', '18039971000100', '2022-04-04 15:33:39'),
	(7, 'Franche', '18039971000100', '2022-04-04 15:34:59'),
	(8, 'REGIADM', '18039971000100', '2022-04-06 11:59:08'),
	(9, 'TesteAdm2', '18039971000100', '2022-04-07 16:06:11'),
	(10, 'TesteAdm', '23399129340002', '2022-04-11 10:27:28'),
	(11, 'TesteAdm', '23399129340002', '2022-04-11 10:27:39');
/*!40000 ALTER TABLE `jp_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_bloco
CREATE TABLE IF NOT EXISTS `jp_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBloco` varchar(50) NOT NULL DEFAULT '',
  `from_condominio` int(11) NOT NULL,
  `Andares` int(11) NOT NULL DEFAULT 0,
  `qtUnidadesAndar` varchar(50) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fkCondominioNomeBloco` (`from_condominio`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_bloco: ~18 rows (aproximadamente)
DELETE FROM `jp_bloco`;
/*!40000 ALTER TABLE `jp_bloco` DISABLE KEYS */;
INSERT INTO `jp_bloco` (`id`, `nomeBloco`, `from_condominio`, `Andares`, `qtUnidadesAndar`, `dataCadastro`) VALUES
	(1, 'A', 0, 4, '5', '2022-03-29 14:22:45'),
	(2, 'A', 3, 4, '5', '2022-03-29 14:22:45'),
	(3, 'A', 1, 4, '5', '2022-03-29 14:22:45'),
	(5, 'D', 1, 5, '19', '2022-04-01 08:52:39'),
	(6, 'B', 1, 5, '3', '2022-04-01 08:52:57'),
	(7, 'CA', 1, 5, '6', '2022-04-01 08:53:06'),
	(9, 'BB', 1, 6, '23', '2022-04-01 09:56:49'),
	(14, 'BB', 2, 6, '12', '2022-04-01 09:56:49'),
	(15, 'CD', 2, 3, '5', '2022-04-01 09:56:49'),
	(16, 'CD', 3, 3, '5', '2022-04-01 09:56:49'),
	(17, 'A', 1, 4, '5', '2022-03-29 14:22:45'),
	(18, 'A', 1, 4, '5', '2022-03-29 14:22:45'),
	(19, 'A', 1, 4, '5', '2022-03-29 14:22:45'),
	(20, 'A', 1, 4, '5', '2022-03-29 14:22:45'),
	(25, 'Bloco 01', 38, 123, '213', '2022-04-11 11:48:32'),
	(26, 'Bloco 02', 8, 123, '213', '2022-04-11 11:49:22'),
	(27, 'Bloco 01', 2, 123, '123', '2022-04-11 11:51:10'),
	(28, '123', 3, 123, '123', '2022-04-11 15:39:51');
/*!40000 ALTER TABLE `jp_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_condominio
CREATE TABLE IF NOT EXISTS `jp_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT '',
  `from_administradora` int(11) DEFAULT NULL,
  `from_bloco` int(11) DEFAULT NULL,
  `from_sindico` int(11) DEFAULT NULL,
  `qtblocos` int(11) NOT NULL DEFAULT 0,
  `rua` varchar(255) NOT NULL DEFAULT '',
  `num` int(11) NOT NULL DEFAULT 0,
  `bairro` varchar(50) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `estado` varchar(50) NOT NULL DEFAULT '',
  `cep` varchar(8) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chAdm` (`from_administradora`),
  KEY `chBlocos` (`from_bloco`),
  KEY `chSindico` (`from_sindico`),
  CONSTRAINT `chAdm` FOREIGN KEY (`from_administradora`) REFERENCES `jp_administradora` (`id`),
  CONSTRAINT `chBlocos` FOREIGN KEY (`from_bloco`) REFERENCES `jp_bloco` (`id`),
  CONSTRAINT `chSindico` FOREIGN KEY (`from_sindico`) REFERENCES `jp_conselho` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_condominio: ~12 rows (aproximadamente)
DELETE FROM `jp_condominio`;
/*!40000 ALTER TABLE `jp_condominio` DISABLE KEYS */;
INSERT INTO `jp_condominio` (`id`, `nome`, `from_administradora`, `from_bloco`, `from_sindico`, `qtblocos`, `rua`, `num`, `bairro`, `cidade`, `estado`, `cep`, `dataCadastro`) VALUES
	(1, 'Cond. Teste', 1, 1, NULL, 2, 'Rua teste', 123, 'teste', 'teste', 'SC', '89032328', '2022-03-29 14:21:03'),
	(2, 'Cond. Comercial Gabriel', 1, 1, 1, 2, 'Rua teste', 111, 'teste', 'teste', 'SC', '89032328', '2022-03-29 14:21:03'),
	(3, 'Cond. Teste2', 1, 1, NULL, 3, 'Rua teste', 122, 'teste', 'teste', 'SC', '89032328', '2022-03-29 14:21:03'),
	(4, 'Vasco Da Gama', 2, 1, NULL, 2, 'Rua teste', 312, 'teste', 'teste', 'SC', '89032128', '2022-03-30 13:10:35'),
	(5, 'Ville de Ap', 3, 1, NULL, 2, 'Rua teste', 367, 'teste', 'teste', 'SC', '89032128', '2022-03-30 13:12:24'),
	(6, 'Reginaldo Residence', 1, 1, 1, 1, 'Rua teste', 121, 'teste', 'teste', 'GO', '89032128', '2022-03-30 13:12:41'),
	(7, 'Mbappe Residence', 3, 2, NULL, 2, 'Rua teste', 552, 'teste', 'teste', 'SC', '89032128', '2022-03-30 13:13:52'),
	(8, 'Casemiro Residence', 2, 2, NULL, 2, 'Rua teste', 223, 'teste', 'teste', 'SC', '89032128', '2022-03-30 13:13:52'),
	(34, 'WhatsApp Residence', 2, 2, NULL, 2, 'Rua teste', 552, 'teste', 'teste', 'SC', '89032128', '2022-03-30 13:13:52'),
	(38, 'asdasda', 3, NULL, NULL, 123123, 'asdasda', 2, 'asdasd', 'asdasd', '', '12323-12', '2022-04-11 11:04:35'),
	(39, 'asdasda', 1, NULL, NULL, 2, 'asdasda', 2, 'asdasd', 'asdasd', '', '12323-12', '2022-04-11 11:04:52'),
	(40, 'asdasda', 2, NULL, NULL, 2, 'asdasda', 23, '432', 'asdasd', '', '12323-12', '2022-04-11 15:08:13');
/*!40000 ALTER TABLE `jp_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_conselho
CREATE TABLE IF NOT EXISTS `jp_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NomeFunc` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('Conselheiro','Sindico','SubSindico') NOT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominioparaConselho` (`from_condominio`),
  CONSTRAINT `chCondominioparaConselho` FOREIGN KEY (`from_condominio`) REFERENCES `jp_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_conselho: ~9 rows (aproximadamente)
DELETE FROM `jp_conselho`;
/*!40000 ALTER TABLE `jp_conselho` DISABLE KEYS */;
INSERT INTO `jp_conselho` (`id`, `NomeFunc`, `funcao`, `from_condominio`, `dataCadastro`) VALUES
	(1, 'Joao', 'Conselheiro', 1, '2022-03-30 16:25:38'),
	(2, 'Ronaldinho', 'Conselheiro', 1, '2022-03-30 16:25:38'),
	(3, 'Brabao', 'Conselheiro', 1, '2022-03-30 16:25:38'),
	(4, 'Donald', 'Sindico', 1, '2022-03-30 16:25:38'),
	(5, 'Cleiton', 'Sindico', 2, '2022-03-30 16:25:38'),
	(6, 'Will', 'SubSindico', 2, '2022-03-30 16:25:38'),
	(7, 'Joelson', 'Conselheiro', 4, '2022-03-30 16:25:38'),
	(8, 'Smith', 'Conselheiro', 4, '2022-03-30 16:25:38'),
	(9, 'Cleber', 'SubSindico', 4, '2022-03-30 16:25:38');
/*!40000 ALTER TABLE `jp_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_morador
CREATE TABLE IF NOT EXISTS `jp_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL DEFAULT 0,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `nomeMorador` varchar(50) NOT NULL DEFAULT '0',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(50) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`from_condominio`),
  KEY `chBloco` (`from_bloco`),
  KEY `chUnidade` (`from_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`from_bloco`) REFERENCES `jp_bloco` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `jp_condominio` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `jp_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_morador: ~18 rows (aproximadamente)
DELETE FROM `jp_morador`;
/*!40000 ALTER TABLE `jp_morador` DISABLE KEYS */;
INSERT INTO `jp_morador` (`id`, `from_condominio`, `from_bloco`, `from_unidade`, `nomeMorador`, `cpf`, `email`, `telefone`, `dataCadastro`, `dataUpdate`, `nascimento`) VALUES
	(1, 1, 3, 4, 'joao', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:01:47', '2022-04-01 14:54:07', NULL),
	(2, 1, 1, 1, 'Marcely', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:24', '2022-04-01 14:54:07', NULL),
	(3, 1, 1, 1, 'Cleitin', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:27', '2022-04-01 14:54:07', NULL),
	(5, 2, 1, 1, 'Pedro', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:26', '2022-04-01 14:54:07', NULL),
	(9, 1, 1, 1, 'Wesley', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:28', '2022-04-01 14:54:07', NULL),
	(19, 1, 1, 1, 'asdasd', '06027548959', 'adas@gmail.c', '47991620312', '2022-04-11 09:52:28', '2022-04-04 11:48:23', NULL),
	(21, 1, 1, 1, 'asdasd', '06027548959', 'adas@gmail.c', '47991620312', '2022-04-11 09:52:29', '2022-04-04 11:48:41', NULL),
	(22, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:30', '2022-04-01 14:54:07', NULL),
	(23, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:30', '2022-04-01 14:54:07', NULL),
	(24, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:31', '2022-04-01 14:54:07', NULL),
	(25, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:31', '2022-04-01 14:54:07', NULL),
	(29, 1, 1, 1, 'asdasd', '06027548959', 'adas@gmail.c', '47991620312', '2022-04-11 09:52:32', '2022-04-06 11:55:43', NULL),
	(32, 1, 1, 1, 'asdasd', '06027548959', 'adas@gmail.c', '47991620312', '2022-04-11 09:52:32', '2022-04-07 08:42:21', NULL),
	(33, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:33', '2022-04-01 14:54:07', NULL),
	(34, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '47991620312', '2022-04-11 09:52:34', '2022-04-01 14:54:07', NULL),
	(35, 1, 1, 1, 'Lucas', '06027548959', 'joaopedro@gmail', '4799162031', '2022-04-11 09:52:34', '2022-04-01 14:54:07', NULL),
	(41, 1, 1, 1, 'MBAPPE', '06027548959', '123123@132.com', '34243243', '2022-04-11 13:31:43', '2022-04-11 13:31:43', NULL),
	(42, 2, 2, 25, 'Gabriel', '09024542959', '3123123@gmail.com', '34243243', '2022-04-11 13:33:27', '2022-04-11 13:33:27', NULL),
	(43, 1, 3, 28, 'João Pedro Oechsler', '06027548959', 'joaopedro@gmail.com', '479916-20312', '2022-04-12 13:54:32', '2022-04-12 13:49:29', '0000-00-00'),
	(44, 1, 3, 28, 'asdasd', '06027548959', 'adas@gmail.c', '34243243', '2022-04-12 14:51:19', '2022-04-12 14:51:19', '0000-00-00');
/*!40000 ALTER TABLE `jp_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_pet
CREATE TABLE IF NOT EXISTS `jp_pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomepet` varchar(50) DEFAULT NULL,
  `tipo` enum('Cachorro','Gato','Passaro') DEFAULT NULL,
  `from_morador` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chMoradorPet` (`from_morador`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_pet: ~0 rows (aproximadamente)
DELETE FROM `jp_pet`;
/*!40000 ALTER TABLE `jp_pet` DISABLE KEYS */;
/*!40000 ALTER TABLE `jp_pet` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_unidade
CREATE TABLE IF NOT EXISTS `jp_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numUnd` varchar(50) NOT NULL DEFAULT '',
  `from_morador` int(15) DEFAULT NULL,
  `from_bloco` int(11) DEFAULT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  `metragem` float NOT NULL DEFAULT 0,
  `qtVagas` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chMoradores` (`from_morador`),
  KEY `chBlocosUnd` (`from_bloco`),
  KEY `chCondominioUnd` (`from_condominio`),
  CONSTRAINT `chBlocosUnd` FOREIGN KEY (`from_bloco`) REFERENCES `jp_bloco` (`id`),
  CONSTRAINT `chCondominioUnd` FOREIGN KEY (`from_condominio`) REFERENCES `jp_condominio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `chMoradores` FOREIGN KEY (`from_morador`) REFERENCES `jp_morador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_unidade: ~5 rows (aproximadamente)
DELETE FROM `jp_unidade`;
/*!40000 ALTER TABLE `jp_unidade` DISABLE KEYS */;
INSERT INTO `jp_unidade` (`id`, `numUnd`, `from_morador`, `from_bloco`, `from_condominio`, `metragem`, `qtVagas`, `dataCadastro`) VALUES
	(1, '101', 1, 7, 3, 12, 6, '2022-04-12 08:51:15'),
	(2, '102', 2, 1, 2, 45, 0, '2022-03-30 14:14:36'),
	(4, '104', 3, 1, 3, 29, 0, '2022-03-30 14:14:41'),
	(25, '104', 3, 1, 3, 29, 0, '2022-03-30 14:14:41'),
	(28, '123', NULL, 3, 2, 123, 123, '2022-04-11 13:13:02');
/*!40000 ALTER TABLE `jp_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.jp_user
CREATE TABLE IF NOT EXISTS `jp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.jp_user: ~3 rows (aproximadamente)
DELETE FROM `jp_user`;
/*!40000 ALTER TABLE `jp_user` DISABLE KEYS */;
INSERT INTO `jp_user` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(2, 'jao', 'jao', '123', '2022-04-06 15:50:18'),
	(28, 'asd', 'asd', '202cb962ac59075b964b07152d234b70', '2022-04-11 07:59:18'),
	(29, 'asdasdasdd', 'asd123', '202cb962ac59075b964b07152d234b70', '2022-04-11 08:14:22');
/*!40000 ALTER TABLE `jp_user` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.lista_de_convidados
CREATE TABLE IF NOT EXISTS `lista_de_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(50) NOT NULL DEFAULT '0',
  `cpf` int(11) NOT NULL DEFAULT 0,
  `celular` int(11) NOT NULL DEFAULT 0,
  `from_reserva_salao_festas` int(11) NOT NULL DEFAULT 0,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fkUnidade` (`from_unidade`),
  KEY `fkReserva` (`from_reserva_salao_festas`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.lista_de_convidados: ~2 rows (aproximadamente)
DELETE FROM `lista_de_convidados`;
/*!40000 ALTER TABLE `lista_de_convidados` DISABLE KEYS */;
INSERT INTO `lista_de_convidados` (`id`, `convidado`, `cpf`, `celular`, `from_reserva_salao_festas`, `from_unidade`) VALUES
	(1, 'Ronaldinho', 2147483647, 2147483647, 6, 11),
	(2, 'Ronaldinho', 2147483647, 2147483647, 6, 11);
/*!40000 ALTER TABLE `lista_de_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela jp_php.reserva_salao_festas
CREATE TABLE IF NOT EXISTS `reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_evento` varchar(100) NOT NULL DEFAULT '0',
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `datahora` datetime NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fkUnidade1` (`from_unidade`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jp_php.reserva_salao_festas: ~5 rows (aproximadamente)
DELETE FROM `reserva_salao_festas`;
/*!40000 ALTER TABLE `reserva_salao_festas` DISABLE KEYS */;
INSERT INTO `reserva_salao_festas` (`id`, `titulo_evento`, `from_unidade`, `datahora`, `dataCadastro`) VALUES
	(4, 'teste', 1, '2022-03-30 17:13:18', '2022-03-30 17:13:19'),
	(5, 'Festa nsei', 12, '2022-03-30 17:13:18', '2022-03-30 17:13:19'),
	(6, 'Party', 5, '2022-03-30 17:13:18', '2022-03-30 17:13:19'),
	(12, 'Festinha doida', 4, '2022-03-30 17:13:18', '2022-03-30 17:13:19'),
	(13, 'Bolada', 8, '2022-03-30 17:13:18', '2022-03-30 17:13:19');
/*!40000 ALTER TABLE `reserva_salao_festas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

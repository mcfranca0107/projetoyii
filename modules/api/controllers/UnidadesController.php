<?
namespace app\modules\api\controllers;

use app\models\UnidadesModel;
use yii\web\Controller;

class UnidadesController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function actionGetAll(){
        $qry = (new \yii\db\Query())
            ->select('unidade.id, unidade.numUnd, bloco.nomeBloco, condo.nome, unidade.from_condominio, unidade.from_bloco, 
                unidade.metragem, unidade.qtVagas, unidade.dataCadastro')
            ->from('jp_unidade unidade')
            ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');
        
        $data = $qry->orderBy('numUnd')->all();
        $dados = [];
        $i = 0;

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                $dados['totalResults'] = $qry->count();

                foreach($data as $d){
                    foreach($d as $ch=>$r){
                        $dados['resultSet'][$i][$ch] = $r;
                    }
                    $i++;
                }

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
            ->select('unidade.id, unidade.numUnd, bloco.id AS idBloco, bloco.nomeBloco, condo.id AS idCond, condo.nome, unidade.metragem, unidade.qtVagas, 
                unidade.dataCadastro')
            ->from('jp_unidade unidade')
            ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');

        $d = $qry->where(['unidade.id' => $request->get('id')])->one();

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$ch] = $r;
                }
            }
            return json_encode($dados);
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionRegisterUni(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new UnidadesModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionEditUni(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = UnidadesModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso';

                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionDeleteUni(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = UnidadesModel::findOne($request->post('id'));
                $model->delete();
                
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro excluído com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }
}

?>
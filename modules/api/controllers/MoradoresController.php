<?
namespace app\modules\api\controllers;

use app\models\MoradoresModel;
use yii\web\Controller;

class MoradoresController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function actionGetAll(){
        
        $qry = (new \yii\db\Query())
            ->select('morador.id, bloco.nomeBloco, condo.nome, und.numUnd, morador.from_condominio, morador.from_bloco,
                morador.from_unidade, morador.nomeMorador, morador.dataCadastro, morador.cpf, morador.email, morador.telefone,
                morador.nascimento')
            ->from('jp_morador morador')
            ->leftJoin('jp_bloco bloco', 'bloco.id = morador.from_bloco')
            ->leftJoin('jp_condominio condo', 'condo.id = morador.from_condominio')
            ->leftJoin('jp_unidade und', 'und.id = morador.from_unidade');
    
        $data = $qry->orderBy('nomeMorador')->all();
        $dados = [];
        $i = 0;

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                $dados['totalResults'] = $qry->count();

                foreach($data as $d){
                    foreach($d as $ch=>$r){
                        $dados['resultSet'][$i][$ch] = $r;
                    }
                    $i++;
                }
                
                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionGetOne(){
        $request = \yii::$app->request;

        $qry = (new \yii\db\Query())
            ->select('morador.id, bloco.nomeBloco, condo.nome, und.numUnd, morador.from_condominio, morador.from_bloco,
                morador.from_unidade, morador.nomeMorador, morador.dataCadastro, morador.cpf, morador.email, morador.telefone,
                morador.nascimento')
            ->from('jp_morador morador')
            ->leftJoin('jp_bloco bloco', 'bloco.id = morador.from_bloco')
            ->leftJoin('jp_condominio condo', 'condo.id = morador.from_condominio')
            ->leftJoin('jp_unidade und', 'und.id = morador.from_unidade');

        $d = $qry->where(['morador.id' => $request->get('id')])->one();

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$ch] = $r;
                }
            }
            return json_encode($dados);
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
            $dados['endPoint']['erro'] = $th;
        }    
    }

    public function actionRegisterMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new MoradoresModel();
                $model -> attributes = $request->post();
                $model -> save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionEditMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso';

                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }
    
    public function actionDeleteMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->delete();
                
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro excluído com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

}

?>
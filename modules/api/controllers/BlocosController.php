<?
namespace app\modules\api\controllers;

use app\models\BlocosModel;
use yii\web\Controller;

class BlocosController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function actionGetAll(){
        $qry = (new \yii\db\Query())
            ->select('condo.id as idCondo, bloco.id, condo.nome, bloco.nomeBloco, bloco.Andares, bloco.qtUnidadesAndar,
                bloco.dataCadastro')
            ->from('jp_bloco bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = bloco.from_condominio');

        $data = $qry->orderBy('nomeBloco')->all();
        $dados = [];
        $i = 0;

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                $dados['totalResults'] = $qry->count();

                foreach($data as $d){
                    foreach($d as $ch=>$r){
                        $dados['resultSet'][$i][$ch] = $r;
                    }
                    $i++;
                }
                
                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
            ->select('condo.id as idCondo, bloco.id, condo.nome, bloco.nomeBloco, bloco.Andares, bloco.qtUnidadesAndar,
                bloco.dataCadastro')
            ->from('jp_bloco bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = bloco.from_condominio');
        
        $d = $qry->where(['bloco.id' => $request->get('id')])->one();

        try {
            if($qry->count() > 0){
                $dados['endPoint']['status'] = 'success';
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$ch] = $r;
                }
            }
            return json_encode($dados);

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
            $dados['endPoint']['erro'] = $th;
        }
    }

    public function actionRegisterBlocos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new BlocosModel();
                $model -> attributes = $request->post();
                $model -> save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionEditBlocos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = BlocosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso';

                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para esse consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionDeleteBlocos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = BlocosModel::findOne($request->post('id'));
                $model->delete();
                
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro excluído com sucesso';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            $dados['endPoint']['error'] = $th;

            return json_encode($dados);
        }
    }

    public function actionGetBlocoFromCond(){
        $request = \yii::$app->request;
        $qry = BlocosModel::find();

        $data = $qry->where(['from_condominio' => $request->get('from_condominio')]) -> orderBy('nomeBloco')->all();
        $dados = [];

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeBloco'] = $d['nomeBloco'];
                $i++;
            }
        } else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

}

?>
<?

use app\components\selectedComponent;
use app\controllers\CondominiosController;
use yii\helpers\Url;
?>

<div class="row justify-content-center">

    <h1 class="text-center text-dark">Cadastro de Bloco</h1>
    <form id="form-bloco" class="col-12 px-2 py-2 mt-4 mb-4 rounded shadow" action="<?=Url::to(['blocos/realiza-cadastro-blocos']);?>" method="post">

        <select name="from_condominio" class="custom-select mt-2">
            <option value="">Selecione o Condominio</option>
            <?
            foreach (CondominiosController::listaCondominiosSelect() as $condo) {
            ?>
                <option value="<?= $condo['id'] ?>"<?=selectedComponent::isSelected($condo['id'], $edit['from_condominio'])?>><?= $condo['nome'] ?></option>
            <? } ?>
        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="nomeBloco" value="<?= $edit['nomeBloco'] ?>" placeholder="Nome do Bloco" required>
        <input class="col col-12 mt-2 form-control" type="text" name="Andares" value="<?= $edit['Andares'] ?>" placeholder="Andares" required>
        <input class="col col-12 mt-2 form-control" type="text" name="qtUnidadesAndar" value="<?= $edit['qtUnidadesAndar'] ?>" placeholder="Qtd de Unidades por Andar">

        <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">
        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <button class="btn btn-info mt-2 buttonEnviar" type="submit">Salvar</button>
        <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Fechar</button>
    </form>
</div>
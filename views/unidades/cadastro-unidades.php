<?

use app\controllers\BlocosController;
use app\controllers\CondominiosController;
use yii\helpers\Url;

?>

<div class="row justify-content-center">

    <h1 class="text-center text-dark">Cadastro de Unidades</h1>

    <form id="form-unidade" class="col-12 mt-4 mb-4 py-2 px-2 rounded shadow" action="<?= Url::to(['unidades/realiza-cadastro-unidades']); ?>" method="post">

        <input class="col col-12 mt-2 form-control" type="text" name="numUnd" value="" placeholder="Numero da unidade" required>

        <select name="from_condominio" class="custom-select mt-2 fromCondominio">
            <option value="">Selecione o Condominio</option>
            <?
            foreach (CondominiosController::listaCondominiosSelect() as $condo) {
            ?>
                <option value="<?= $condo['id'] ?>"><?= $condo['nome'] ?></option>
            <? } ?>
        </select>

        <select name="from_bloco" class="fromBloco custom-select mt-2">

        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="metragem" value="" placeholder="Metragem" required>
        <input class="col col-12 mt-2 form-control" type="number" name="qtVagas" value="" placeholder="Qtd de Vagas">

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>

    </form>

</div>
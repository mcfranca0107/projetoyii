<?

use app\components\alertComponent;
use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\url;

$url_site = Url::base($schema = true);

// $leg = Yii::$app->legivelComponent;
// $leg->legivel($unidades);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

?>
<h1 class="text-center">Unidades</h1>
<ul>

<table class="container-fluid table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaUnd">
    <tr>
        <td>Condominio</td>
        <td>Bloco</td>
        <td>Numero</td>
        <td>Metragem</td>
        <td>Vagas</td>
        <td>Data Criado</td>
        <td>Data Atualizada</td>
        <td align="center"><a href="index.php?page=cadastroUnidade" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($unidades as $dadosUnd) {
    ?>
        <tr data-id="<?=$dadosUnd['id']?>">
            <td><?= $dadosUnd['nome'] ?></td>
            <td><?= $dadosUnd['nomeBloco'] ?></td>
            <td><?= $dadosUnd['numUnd'] ?></td>
            <td><?= $dadosUnd['metragem'] ?></td>
             <td><?= $dadosUnd['qtVagas'] ?></td>
             <td><?=Yii::$app->formatter->format($dadosUnd['dataCadastro'],'date')?></td>
            <td align="center">
                <a href="<?=$url_site?>/index.php?r=unidades/edita-unidades&id=<?=$dadosUnd['id']?>" class="openModal"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="<?=$url_site?>/index.php?r=unidades/deleta-unidade&id=<?=$dadosUnd['id']?>" data-id="<?= $dadosUnd['id']?>" class="removerUnd"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
            <td colspan="6"></td>
        </tr>
    <? } ?>
</table>

<div class="totalRegistros col-12 float-right">
    Total Registros <?=$paginacao->totalCount?>
</div>

<div class="row">
    <div class="col-12 mt-2">
            <?= LinkPager::widget(
            [
                'pagination' => $paginacao, 
                'linkContainerOptions' => [
                    'class' => 'page-item bg-dark text-info border-info'
                    ]
                , 'linkOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ]
            ]
            ) ?>
    </div>
</div>

<?=modalComponent::initModal()?>

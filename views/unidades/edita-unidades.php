<?

use app\components\selectedComponent;
use app\controllers\BlocosController;
use app\controllers\CondominiosController;
use yii\helpers\Url;

?>

<div class="row justify-content-center">

    <h1 class="text-center text-dark">Cadastro de Unidades</h1>

    <form id="form-unidade" class="col-12 mt-4 mb-4 py-2 px-2 rounded shadow" action="<?= Url::to(['unidades/realiza-edicao-unidade']); ?>" method="post">

        <input class="col col-12 mt-2 form-control" type="text" name="numUnd" value="<?= $edit['numUnd'] ?>" placeholder="Numero da unidade" required>

        <select name="from_condominio" class="custom-select mt-2 fromCondominio">
            <option value="">Selecione o Condominio</option>
            <?
            foreach (CondominiosController::listaCondominiosSelect() as $condo) {
            ?>
                <option value="<?= $condo['id'] ?>"<?=selectedComponent::isSelected($condo['id'], $edit['from_condominio'])?>><?= $condo['nome'] ?></option>
            <? } ?>
        </select>

        <select name="from_bloco" class="fromBloco custom-select mt-2">
            <?
            foreach (BlocosController::listaBlocosEdit($edit['from_condominio']) as $bloco) {
                echo '<option value="' . $bloco['id'] . '"' . selectedComponent::isSelected($bloco['id'], $edit['from_bloco']) . '>' . $bloco['nomeBloco'] . '</option>';
            }
            ?>
        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="metragem" value="<?= $edit['metragem'] ?>" placeholder="Metragem" required>
        <input class="col col-12 mt-2 form-control" type="number" name="qtVagas" value="<?= $edit['qtVagas'] ?>" placeholder="Qtd de Vagas">

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">
        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <button class="btn btn-info mt-2 buttonEnviar" type="submit">Salvar</button>
        <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Fechar</button>

    </form>

</div>
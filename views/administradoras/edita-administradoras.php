<?
use yii\helpers\Url;
?>

<div class="">

    <h1 class="text-center text-dark">Cadastro de Administradoras</h1>

        <div class="">
            <form id="form-adm" class="col-12 py-2 mt-4 mb-4 rounded shadow" action="<?=Url::to(['administradoras/realiza-edicao-administradora']);?>" method="post">

                <input class="col col-12 mt-2 form-control" type="text" name="nomeAdm" value="<?= $edit['nomeAdm'] ?>" placeholder="Nome da Adm" required>
                
                <input class="col col-12 mt-2 form-control" type="text" name="cnpj" value="<?= $edit['cnpj'] ?>" placeholder="CNPJ"required>       

                <input type="hidden" name="<?=yii::$app->request->csrfParam;?>" value="<?=yii::$app->request->csrfToken;?>">
                <input type="hidden" name="id" value="<?=$edit['id']?>">

                <button class="btn btn-info mt-2 buttonEnviar" type="submit">Salvar</button>
                <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Fechar</button>
            </form>
        </div>
        
</div>
<?
//error_reporting('E_FATAL | E_PARSE' );

use app\components\alertComponent;
use yii\helpers\Url;

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Gestão de Clientes</title>
</head>

<body class="login">
    <img src="imagens/logo.png" alt="">
    <main class="container">
        <div class="row">
            <div class="caixaLogin">
                <?=(isset($_GET['myAlert'])) ? alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'], $_GET['myAlert']['redir']) : '';?>
                <form class="form-signin" action="<?=Url::to(['site/login']);?>" method="POST">
                    <h1 class="h3 mb-3"><b>Área de login</b></h1>
                    
                    <label for="usuario" class="sr-only">Usuário</label>
                    <input type="text" id="usuario" class="form-control mb-3" name="usuario" placeholder="Usuário" required autofocus>
                    
                    <label for="senha" class="sr-only">Senha</label>
                    <input type="password" id="senha" class="form-control mb-3" name="senha" placeholder="Senha" required>
                    
                    <input type="hidden" name="<?=\yii::$app->request->csrfParam;?>" value="<?=\yii::$app->request->csrfToken;?>">
                    <button class="btn btn-lg btn-dark btn-block" type="submit">Entrar</button>
                </form>
            </div>
        </div>
    </main>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js?v=<?=rand(0,9999)?>"></script>
</body>

</html>
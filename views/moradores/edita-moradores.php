<?

use app\components\generoComponent;
use app\components\modalComponent;
use app\components\selectedComponent;
use app\controllers\BlocosController;
use app\controllers\CondominiosController;
use app\controllers\UnidadesController;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use kartik\widgets\FileInput;

?>

<h1 class="text-center text-dark">Cadastro de Moradores</h1>
<form id="form-clientes" class="col-12 px-2 py-2 mt-4 mb-4 rounded shadow" action="<?= Url::to(['moradores/realiza-edicao-morador']); ?>" method="post">

    <select name="from_condominio" class="custom-select mt-2 fromCondominio">
    <option value="">Selecione um Condomínio</option>
        <? 
        foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
        <option value="<?=$cond['id']?>"<?=selectedComponent::isSelected($cond['id'], $edit['from_condominio'])?>><?=$cond['nome']?></option>
        <? } ?>
    </select>

    <select name="from_bloco" class="fromBloco custom-select mt-2">
        <?
        foreach (BlocosController::listaBlocosEdit($edit['from_condominio']) as $bloco) {
            echo '<option value="' . $bloco['id'] . '"' . selectedComponent::isSelected($bloco['id'], $edit['from_bloco']) . '>' . $bloco['nomeBloco'] . '</option>';
        }
        ?>
    </select>

    <select name="from_unidade" class="fromUnidade custom-select mt-2">
        <?
        foreach(UnidadesController::listaUnidadesSelect($edit['from_bloco']) as $und){
            echo '<option value="'.$und['id'].'"'.selectedComponent::isSelected($und['id'], $edit['from_unidade']).'>'.$und['numUnd'].'</option>';
        }
        ?>
    </select>

    <input class="col col-12 mt-2 form-control" type="text" name="nomeMorador" value="<?= $edit['nomeMorador'] ?>" placeholder="Nome" required>

    <input class="col col-12 mt-2 form-control px-0" type="date" name="nascimento"  value="<?= $edit['nascimento'] ?>">


    <div class="col col-12 mt-2 form-group px-0">
        <?= generoComponent::genSelect('genero', $edit['genero']) ?>
    </div>

    <input class="col col-12 mt-2 form-control" type="text" name="cpf" value="<?= $edit['cpf'] ?>" placeholder="CPF" required>
    <input class="col col-12 mt-2 form-control" type="email" name="email" value="<?= $edit['email'] ?>" placeholder="Email" required>
    <input class="col col-12 mt-2 form-control" type="text" name="telefone" value="<?= $edit['telefone'] ?>" placeholder="Telefone">

    <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <button class="btn btn-info mt-2 buttonEnviar" type="submit">Salvar</button>
    <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Fechar</button>
</form>

</div>
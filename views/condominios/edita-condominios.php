<?
use app\components\estadosComponent;
use app\components\selectedComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\controllers\AdministradorasController;
?>

<h1 class="text-center text-dark">Cadastro de Condominios</h1>

<form id="form-condo" class="col-12 py-2 mt-4 mb-4 rounded shadow" action="<?= Url::to(['condominios/realiza-edicao-condominio']); ?>" method="post">

    <input class="col col-12 mt-2 form-control" type="text" name="nome" value="<?= $edit['nome'] ?>" placeholder="Nome" required>
    <input class="col col-12 mt-2 form-control" type="text" name="qtblocos" value="<?= $edit['qtblocos'] ?>" placeholder="Quantidade de Blocos" required>

    <input class="col col-12 mt-4 form-control" type="text" name="rua" value="<?= $edit['rua'] ?>" placeholder="Rua" required>
    <input class="col col-12 mt-2 form-control" type="text" name="num" value="<?= $edit['num'] ?>" placeholder="Número" required>
    <input class="col col-12 mt-2 form-control" type="text" name="bairro" value="<?= $edit['bairro'] ?>" placeholder="Bairro" required>
    <input class="col col-12 mt-2 form-control" type="text" name="cidade" value="<?= $edit['cidade'] ?>" placeholder="Cidade" required>

    <select name="from_administradora" class="col col-12 mt-2 form-control">
        <option value="">Selecione a Administradora</option>
        <?
        foreach (AdministradorasController::listaAdministradorasSelect() as $adm) {
        ?>
            <option value="<?= $adm['id'] ?>"<?=selectedComponent::isSelected($adm['id'], $edit['from_administradora'])?>><?= $adm['nomeAdm'] ?></option>
        <? } ?>
    </select>

    <select id="inputState" class="col col-12 mt-2 form-control" name="from_estado" required>
        <option value="">selecionar o Estado</option>
        <?
        foreach (estadosComponent::estados() as $sig => $uf) { ?>
            <option value="<?=$sig?>" <?=selectedComponent::isSelected($sig, $edit['estado'])?>><?= $uf ?></option>
        <? } ?>
    </select>

    <input class="col col-12 mt-2 form-control" type="text" name="cep" value="<?= $edit['cep'] ?>" placeholder="Cep" required>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <button class="btn btn-info mt-2 buttonEnviar" type="submit">Salvar</button>
    <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Fechar</button>
</form>
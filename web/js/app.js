$(function(){

    $(document).on('change', '.fromCondominio', function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: '?r=blocos/lista-blocos-api',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data,'nomeBloco');
            }
        })

    })

    //chamar unidades

    $(document).on('change', '.fromBloco',function(){
        selecionado = $(this).val();

        $.ajax({
            url: '?r=unidades/lista-unidades-api',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success: function(data){
                selectPopulation('.fromUnidade',data,'numUnd')
            }
        })
    })

    function selectPopulation(seletor, dados, field){
        
        estrutura = '<option value="">Selecione...</option>';
        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }
        $(seletor).html(estrutura)
    }

    //controla filtro
    // $('#filtro').submit(function(){
    //     var pagina = $('input[name="page"]').val();
    //     var termo1 = $('.termo1').val();
    //     var termo2 = $('.termo2').val();

    //     termo1 = (termo1) ? termo1+'/' : '';
    //     termo2 = (termo2) ? termo2+'/' : ''

    //     window.location.href = url_site+pagina+'/busca/'+termo1+termo2

    //     return false;
    // })

    // $('.termo1, .termo2').on('keyup focusout change',function(){
    //     var termo1 = $('.termo1').val();
    //     var termo2 = $('.termo2').val();
    //     if(termo1 || termo2){
    //         $('button[type="submit"]').prop('disabled', false);
    //     }else{
    //         $('button[type="submit"]').prop('disabled', true);
    //     }
    // })

    $('input[name=CPF]').mask('000.000.000-00', {reverse: true});
    $('input[name=cep]').mask('00000-000');

    $(document).on('change', '.actionGenero', function(){
        var gen = $(this).val();
        var name = $(this).attr('id');
        if(gen == 'O'){
             $(this).attr('name','');
             $(this).parent().append('<input class="form-group outroGenero" type="text" name="'+name+'">');
        }else{
             $('.outroGenero').remove();
             $(this).attr('name',name);
        }
    })
    
     //controle do modal
     $('.openModal').click(function(){
         caminho = $(this).attr('href');
         $(".modal-body").load(caminho, function (response, status) {
    
             if (status === "success") {
                 $('#modalComponent').modal({show: true});
             }
         });
    
         return false;
     })

});

function myAlert(tipo, mensagem, pai, url){

    url = (url == undefined) ? url == '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function() {
        $(pai).find('div.alert').remove();
        //vai redir?
        if(tipo == 'success' && url) {
            setTimeout(function(){
                window.location.href = url;
            }, 80);
        }

    }, 2100);

}
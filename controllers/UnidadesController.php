<?
namespace app\controllers;

use app\models\UnidadesModel;
use Yii;
use yii\web\Controller;         
use yii\data\Pagination;


class UnidadesController extends Controller {

    public static function listaUnidadesSelect($from) {
        $query = UnidadesModel::find();
        $data = $query->where(['from_bloco' => $from])->orderBy('numUnd')->all();
        return $data;
    }

    public function actionListaUnidadesApi(){
        $request = \yii::$app->request;
        $query = UnidadesModel::find();
        $data = $query->where(['from_bloco' => $request->post()])->orderBy('numUnd')->all();

        $dadosUnd = array();
        $i = 0;

        foreach($data as $d) {
            $dadosUnd[$i]['id'] = $d['id'];
            $dadosUnd[$i]['numUnd'] = $d['numUnd'];
            $i++;
        }
        return json_encode($dadosUnd);
    }

    public function actionCadastroUnidades() {
        return $this->render('cadastro-unidades');
    }

    public function actionRealizaCadastroUnidades(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/listar-unidades']);
        }

        return $this->render('cadastro-unidades');
    }

    public function actionEditaUnidades() {
        $this->layout = false;
        $request = \yii::$app->request;

        if ($request->isGet) {
            $query = UnidadesModel::find();
            $unidades = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-unidades', [
            'edit' => $unidades
        ]);
    }

    public function actionRealizaEdicaoUnidade(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = UnidadesModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
            if ($model->update()){
                return $this->redirect(['unidades/listar-unidades']);
            } else{
                return $this->redirect(['unidades/listar-unidades', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaUnidade(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = UnidadesModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['unidades/listar-unidades', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado com sucesso.']]);
            } else{
                return $this->redirect(['unidades/listar-unidades', 'myAlert' => ['type' => 'success', 'msg' => 'Não foi possível deletar o registro.']]);
            }
        }
    }

    public function actionListarUnidades() {

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = (new \yii\db\Query())
        ->select('unidade.id, 
        unidade.numUnd, 
        bloco.nomeBloco, 
        condo.nome, 
        unidade.from_condominio, 
        unidade.from_bloco, 
        unidade.metragem, 
        unidade.qtVagas,
        unidade.dataCadastro'
        )
        ->from('jp_unidade unidade')
        ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
        ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $unidades = $query->orderBy('numUnd')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-unidades',[
            'unidades' => $unidades,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>
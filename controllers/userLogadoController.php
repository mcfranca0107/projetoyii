<?
namespace app\controllers;

use Yii;
use Yii\base\Controller;

class userLogado extends Controller{

    public function isLogado(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
    }
}

?>
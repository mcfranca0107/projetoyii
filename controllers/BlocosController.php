<?
namespace app\controllers;

use app\models\BlocosModel;
use Yii;
use yii\web\Controller;         
use yii\data\Pagination;


class BlocosController extends Controller {

    public static function listaBlocosSelect(){
        $query = BlocosModel::find();

        return $query->orderBy('id')->all();
    }

    public static function listaBlocosEdit($from) {
        $query = BlocosModel::find();
        $data = $query->where(['from_condominio' => $from])->orderBy('nomeBloco')->all();
        return $data;
    }

    public function actionEditaBlocos() {
        $this->layout = false;
        $request = \yii::$app->request;

        if ($request->isGet) {
            $query = BlocosModel::find();
            $blocos = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-blocos', [
            'edit' => $blocos
        ]);
    }

    public function actionRealizaEdicaoBlocos(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = BlocosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
            if ($model->update()){
                return $this->redirect(['blocos/listar-blocos']);
            } else{
                return $this->redirect(['blocos/listar-blocos']);
            }
        }
    }

    public function actionDeletaBlocos(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = BlocosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['blocos/listar-blocos', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado com sucesso.']]);
            } else{
                return $this->redirect(['blocos/listar-blocos', 'myAlert' => ['type' => 'danger', 'msg' => 'Não foi possível deletar o registro.']]);
            }
        }
    }

    public function actionListaBlocosApi(){
        $request = \yii::$app->request;
        $query = BlocosModel::find();
        $data = $query->where(['from_condominio' => $request->post()])->orderBy('nomeBloco')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d) {
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeBloco'] = $d['nomeBloco'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionCadastroBlocos() {
        return $this->render('cadastro-blocos');
    }

    public function actionRealizaCadastroBlocos(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new BlocosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['blocos/cadastro-blocos']);
        }

        return $this->render('cadastro-blocos');
    }

    public function actionListarBlocos() {

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = (new \yii\db\Query())
        ->select(
        'condo.id as idCondo,
        bloco.id,
        condo.nome,
        bloco.nomeBloco,
        bloco.Andares,
        bloco.qtUnidadesAndar,
        bloco.dataCadastro'
        )
        ->from('jp_bloco bloco')
        ->innerJoin('jp_condominio condo', 'condo.id = bloco.from_condominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $blocos = $query->orderBy('nomeBloco')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-blocos',[
            'blocos' => $blocos,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>
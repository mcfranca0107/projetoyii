<?
namespace app\controllers;

use app\models\AdministradorasModel;
use Yii;
use yii\web\Controller;         
use yii\data\Pagination;

class AdministradorasController extends Controller {

    public static function listaAdministradorasSelect() {
        $query = AdministradorasModel::find();

        return $query->orderBy('id')->all();
    }

    public function actionCadastraAdministradoras() {
        return $this->render('cadastro-administradoras');
    }

    public function actionRealizaCadastroAdministradora(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new AdministradorasModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['administradoras/listar-administradoras']);
        }

        return $this->render('cadastro-administradoras');
    }

    public function actionEditaAdministradoras() {
        $this->layout = false;
        $request = \yii::$app->request;

        if ($request->isGet) {
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-administradoras', [
            'edit' => $administradoras
        ]);
    }

    public function actionRealizaEdicaoAdministradora(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = AdministradorasModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
            if ($model->update()){
                return $this->redirect(['administradoras/listar-administradoras']);
            } else{
                return $this->redirect(['administradoras/listar-administradoras']);
            }
        }
    }

    public function actionDeletaAdministradora(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = AdministradorasModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['administradoras/listar-administradoras', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado com sucesso.']]);
            } else{
                return $this->redirect(['administradoras/listar-administradoras', 'myAlert' => ['type' => 'danger', 'msg' => 'Não foi possível deletar o registro.']]);
            }
        }
    }

    public function actionListarAdministradoras() {

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = AdministradorasModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $administradoras = $query->orderBy('nomeAdm')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-administradoras',[
            'administradoras' => $administradoras,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>
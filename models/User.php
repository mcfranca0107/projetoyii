<?
namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{
    public static function tableName(){
        return 'jp_user';
    }

    /**
     * Este método procura por uma instância da classe de identidade usando o ID de usuário especificado.
     * Este método é usado quando você precisa manter o status de login via sessão
     * @param [type] $id
     * @return void
     */

    public static function findIdentity($id){
        return static::findOne($id);
    }

    /**
     * Ele procura por uma instância da classe de identidade usando o token de acesso informado.
     * Este método é usado quando você precisa autenticar um usuário por um único token secreto (exemplo: em uma aplicação stateless RESTful);
     * 
     * @param [type] $token
     * @param [type] $type
     * @return void
     */

    public static function findIdentityByAccessToken($token, $type = null){
        return null; //static::findOne(['token' => $token]);
    }

    /**
     * Retorna o id do usuário representado por essa instância da classe de identidade;
     * 
     * @return void
     * 
     */

    public function getId(){
        return $this->id;
    }

    /**
     * Retorna uma chave para verificar login via cookie. A chave é mantida no cookie de login e será comparada com a informação do lado do servidor para testar a validade do cookie.
     * 
     * @return void
     */

    public function getAuthKey(){
        return null; // $this->auth_key;
    }

    /**
     * Implementa a lógica de verificação da chave de login via cookie;
     * 
     * @param [type] $authKey
     * @return void
     */

    public function validateAuthKey($authKey){
        return null; //$this->auth_key === $authKey;
    }
}

?>
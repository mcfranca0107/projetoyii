<?
namespace app\models;

use yii\db\ActiveRecord;

Class BlocosModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_bloco';
    }

    public function rules()
    {
        return [
            [['from_condominio', 'nomeBloco', 'Andares', 'qtUnidadesAndar'], 'required']
        ];
    }
}

?>
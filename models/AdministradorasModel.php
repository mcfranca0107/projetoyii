<?
namespace app\models;

use yii\db\ActiveRecord;

Class AdministradorasModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_administradora';
    }

    public function rules()
    {
        return [
            [['nomeAdm', 'nomeAdm'], 'required'],
            [['cnpj', 'cnpj'], 'required']
        ];
    }
}
?>




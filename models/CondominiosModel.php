<?
namespace app\models;

use yii\db\ActiveRecord;

Class CondominiosModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_condominio';
    }

    public function rules()
    {
        return [
            [['nome', 'qtblocos', 'rua', 'num', 'bairro', 'estado', 'cidade', 'cep'], 'required'],
            [['from_administradora', 'from_administradora'], 'required']

            
        ];
    }
}

?>
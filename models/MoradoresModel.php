<?
namespace app\models;

use yii\db\ActiveRecord;

Class MoradoresModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_morador';
    }

    public function rules()
    {
        return [
            [['from_condominio', 'from_bloco', 'from_unidade', 'genero', 'nomeMorador', 'cpf', 'email', 'telefone', 'nascimento'], 'required'] 
        ];
    }

}

?>